using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ArrowForOxygen : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject arrow;
    private int count = 0;
    private GameObject diver;
    private float delta_x;
    private float delta_y;
    private void Start()
    {
        diver = transform.parent.Find("Main Camera").gameObject;
        delta_x = diver.transform.localPosition.x - transform.localPosition.x;
        delta_y = diver.transform.localPosition.y - transform.localPosition.y;
    }

    private void Update()
    {
        transform.localPosition = new Vector2(
            diver.transform.localPosition.x - delta_x,
            diver.transform.localPosition.y - delta_y);
        transform.localRotation = Quaternion.Euler(Vector3.zero);
    }

    private void OnTriggerEnter2D(Collider2D other)
    { 
        if (other.gameObject.name.StartsWith("Circle"))
        {
            count++;
            arrow.SetActive(true);
        }
    }
    private void OnTriggerExit2D(Collider2D other)
    { 
        if (other.gameObject.name.StartsWith("Circle"))
        {
            count--;
            if(count <= 0)
            {
                arrow.SetActive(false);
            }
            
        }
    }
}
