using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Rotator : MonoBehaviour //, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    public GameObject player;

    /*
    private void OnMouseDrag()
    {
        //float RotX = Input.GetAxis("Mouse X") * RotationSpeed * Mathf.Deg2Rad;
        
        Vector2 screenPos = Camera.main.WorldToScreenPoint(transform.position);
        Vector3 vector = Input.GetTouch(0).position - screenPos;
        float angle = Mathf.Atan2(vector.y, vector.x) * Mathf.Rad2Deg;
        
        transform.RotateAround(Vector3.forward, angle);
    }*/
    /*
    private Vector2 startPosition;
    public void OnBeginDrag(PointerEventData eventData)
    {
        
        startPosition = eventData.position - new Vector2(transform.localPosition.x, transform.localPosition.y);
        pressed = true;
        Debug.Log(startPosition );
        //Debug.Log("Drag Begin");
    }

    public void OnDrag(PointerEventData eventData)
    {
        pressed = true;
        angle = (float)(Math.Atan2(startPosition.y, startPosition.x) - Math.Atan2(transform.localPosition.y, transform.localPosition.x));
     
        Debug.Log(angle);   
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        
        Debug.Log("End");
        BaseAngle += angle;
        angle = 0;
    }
*/
    public void SetLeft()
    {
        if (angle - RotationSpeed > -60 && angle - RotationSpeed < 60)
            angle -= RotationSpeed;
        pressed = true;
    }

    public void SetAngle(float _angle)
    {
        angle = _angle;
        BaseAngle = 0;
        autoRotating = 0;
    }

    public void SetRight()
    {
        if (angle + RotationSpeed > -60 && angle + RotationSpeed < 60)
            angle += RotationSpeed;
        pressed = true;
    }

    private void Awake()
    {
        RotationSpeed *= PlayerToWheelKoef;
    }

    private float angle = 0;
    private float BaseAngle = 0;
    public float PlayerToWheelKoef = 1;
    private float RotationSpeed = 0.095f;
    public float WheelToDiverKoef = 5;

    private bool pressed = false;
    private bool Left = false;
    //public float autoRotatingFrames = 20;
    private float startPoseX;
    private float poseDivX;
    private float autoRotating = 0;
    private float _autoRotating = 0;
    [SerializeField (), Range (0f, 1f)]
    private float q = 0.5f;

    private float stopTime = 0;

    // Update is called once per frame
    void Update()
    {
        var ang = player.transform.eulerAngles.z;
        if (ang > 180) ang -= 360;
        float RotateVar = 0;
        foreach (Touch touch in Input.touches)
        {
            //Debug.Log("Touching at: " + touch.position);

            if (touch.phase == TouchPhase.Began)
            {
                startPoseX = touch.position.x;
                _autoRotating = 0;
                stopTime = 0;
            }
            else if (touch.phase == TouchPhase.Moved)
            {
                if (touch.position.y < GetComponent<RectTransform>().rect.height / 2)
                {
                    if ((ang > -60 && ang < 60)
                        || (ang > 60 && -touch.deltaPosition.x * RotationSpeed < 0)
                        || (ang < -60 && -touch.deltaPosition.x * RotationSpeed > 0)
                    )
                    {
                        autoRotating = RotateVar = -touch.deltaPosition.x * RotationSpeed;
                        transform.Rotate(0, 0, RotateVar, Space.World);
                    }
                }
            }
            else if (touch.phase == TouchPhase.Ended)
            {
                //_autoRotating = autoRotating * 1 / autoRotatingFrames;
                _autoRotating = autoRotating;
                //autoRotating = roatingTime;
            }
        }

        //if (Math.Abs(autoRotating) > Math.Abs(_autoRotating * 2) && _autoRotating != 0 && RotateVar == 0)
        if (Math.Abs(autoRotating) > Math.Abs(0.001f) && _autoRotating != 0 && RotateVar == 0)
        {
            autoRotating = _autoRotating * Mathf.Pow(q, stopTime);
            //autoRotating -= _autoRotating * Mathf.Pow(q, stopTime);
            stopTime += Time.deltaTime;
            
            if ((ang > -60 && ang < 60)
                || (ang > 60 && RotateVar < 0)
                || (ang < -60 && RotateVar > 0)
            )
            {
                RotateVar = autoRotating;

                transform.Rotate(0, 0, RotateVar, Space.World);
            }
        }
        

        player.transform.Rotate(0, 0, RotateVar / WheelToDiverKoef, Space.World);
    
    }
}

