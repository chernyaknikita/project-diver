using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;
using RND = System.Random;

public class Spammer : MonoBehaviour
{
/*
    [Serializable]
    public enum RandomStyle
    {
        Equal,
        Gauss,
        AntiGauss
    }
    public RandomStyle style;

    */

    [Serializable]
    public enum SpamStyle
    {
        Distance,
        Timer
    }

    public SpamStyle spamStyle;
    public float spamDistanse;
    public float lastSpamY = -10000;
    private GameObject SpamObject;
    public List<GameObject> SpamObjects;
    private GameObject[] RestartSpamObjects;
    private GameObject _scene;

//    private bool spam = true;
    private bool zeroCheck = false;
    public float timer = 10;

    private void Awake()
    {
        RestartSpamObjects = new GameObject[SpamObjects.Count];

        int i = 0;
        SpamObjects.ForEach((item) =>
        {
            RestartSpamObjects[i] = item;
            i++;
        });
    }

    private void OnEnable()
    {
        //Debug.Log("Enabled");
        lastSpamY = -10000;
        _timer = 0;

        int i = 0;
        foreach (GameObject VARIABLE in RestartSpamObjects)
        {
        
            SpamObjects[i] = VARIABLE ;
            i++;
        }
        
    }

    public void SetStartY()
    {
        //lastSpamY = StartY.y - locPos.y + lastSpamY;
        transform.localPosition = StartY;
        zeroCheck = true;
    }

    public void SetSpam(float minus)
    {
        lastSpamY -= minus;
        zeroCheck = true;
    }

    // Start is called before the first frame update
    private float _timer = 0;
    private float StartZero;
    private Vector3 StartY;

    void Start()
    {
        _scene = GameObject.Find("Canvas Scene").gameObject;

        var tpar = transform.parent;
        transform.SetParent(_scene.transform);
        StartZero = transform.localPosition.y;
        transform.SetParent(tpar);

        StartY = transform.localPosition;
    }
    /*  
      private RND rand = new RND(); //reuse this if you are generating many
      private float GaussRand(float mean, float std)
      {
         
          double u1 = 1.0-rand.NextDouble(); //uniform(0,1] random doubles
          double u2 = 1.0-rand.NextDouble();
          double randStdNormal = Math.Sqrt(-2.0 * Math.Log(u1)) *
                                 Math.Sin(2.0 * Math.PI * u2); //random normal(0,1)
          double randNormal =
              mean + stdDev * randStdNormal; //random normal(mean,stdDev^2)
      }
  */
    // Update is called once per frame

    public Vector3 locPos;

    void Update()
    {
        var delta = Time.deltaTime;
        _timer += delta;
        var tpar = transform.parent;
        transform.SetParent(_scene.transform);
        locPos = transform.localPosition;

        transform.SetParent(tpar);

        if (zeroCheck)
        {
            /*if(locPos.y < StartZero)
                return;
            else
            {
                zeroCheck = false;
            }*/

            zeroCheck = false;
            return;
        }


        if ((spamStyle == SpamStyle.Timer && _timer > timer) ||
            (spamStyle == SpamStyle.Distance && locPos.y > lastSpamY + spamDistanse))
        {
            _timer = 0;
            var size_x = GetComponent<BoxCollider2D>().size.x;
            var size_y = GetComponent<BoxCollider2D>().size.y;

            Vector3 position = Vector3.zero;
            /*if (style == RandomStyle.Equal)
            {*/
            position = new Vector3(
                transform.localPosition.x + GetComponent<BoxCollider2D>().offset.x +
                Random.Range(-size_x / 2, +size_x / 2),
                transform.localPosition.y + GetComponent<BoxCollider2D>().offset.y +
                Random.Range(-size_y / 2, +size_y / 2),
                transform.localPosition.z);
            /*}
            else if (style == RandomStyle.Gauss)
            {
            }
            else if (style == RandomStyle.AntiGauss)
            {
                
            }*/

            SpamObject = SpamObjects[0];
            SpamObjects.RemoveAt(0);
            SpamObjects.Add(SpamObject);
            GameObject go = Instantiate(SpamObject, gameObject.transform.parent, false);
            go.transform.localPosition = position;
            lastSpamY = locPos.y;
            go.transform.SetParent(_scene.transform);
            //Debug.Log(spamStyle + " " + locPos.y + " " + position);
        }
    }
}