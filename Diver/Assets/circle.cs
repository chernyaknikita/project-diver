using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class circle : MonoBehaviour
{
    public float Speed = 10;
    // Start is called before the first frame update
    private float _timer;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        var delta = Time.deltaTime;
        _timer += delta;
        transform.localPosition -= new Vector3(0, Speed* delta, 0);

        if (_timer > 5)
        {
            Destroy(gameObject);
        }
    }
}
