using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.UI;

public class diver : MonoBehaviour
{
    public float Speed;

    public float lifeTime;
    public float circleBonus;
    public float dieTime;
    private float _dieTime;
    public float respawnTime;
    private float _respawnTime;
    private float lifeTime_now;

    private State _state = State.Alive;
    //private GameObject background;
    enum State
    {
        Alive,
        Diying,
        Respauning
    }
    // Start is called before the first frame update
    private float startFill;
    private float starY;
    void Start()
    {
        starY = transform.localPosition.y;
        startFill = GameObject.Find("Canvas UI").transform.Find("Oxygen").gameObject.GetComponent<Image>().fillAmount;
        lifeTime_now = lifeTime;
//        background = transform.parent.Find("background").gameObject;
    }

    private Vector3 frameSpeed;

    public Vector3 GetFrameSpeed()
    {
        return frameSpeed;
    }

    private void OnTriggerEnter2D(Collider2D other)
    { 
        if (other.gameObject.name.StartsWith("Circle"))
        {
            lifeTime_now += circleBonus;
            if (lifeTime_now > lifeTime)
                lifeTime_now = lifeTime;
            
            Destroy(other.gameObject);
        }
    }

    public bool isAlive()
    {
        return _state == State.Alive;
    }

    // Update is called once per frame
    void Update()
    {
        
        //transform.parent.Find("Main Camera").GetComponent<follower>().SetY();
        //return;
        var delta = Time.deltaTime;
        if (_state == State.Alive)
        {

           float areaY = 1000;
            if (transform.localPosition.y > areaY)
            {
                transform.localPosition = 
                    new Vector3(
                        transform.localPosition.x, 
                        //transform.localPosition.y - 2 * areaY,
                        //starY - areaY,
                        starY,
                        transform.localPosition.z
                        );
                foreach (Transform VARIABLE in gameObject.transform.parent)
                {
                    if (VARIABLE.gameObject.name.StartsWith("Circle"))
                    {
                        VARIABLE.transform.localPosition = 
                            new Vector3(
                                VARIABLE.transform.localPosition.x, 
                                //VARIABLE.transform.localPosition.y - 2 * areaY, 
//                                VARIABLE.transform.localPosition.y - areaY *2 + starY, 
                                VARIABLE.transform.localPosition.y - areaY + starY, 
                                VARIABLE.transform.localPosition.z
                                );
                    }
                }
                //GameObject.Find("Background").transform.Find("Spamer").GetComponent<Spammer>().SetSpam(2 * areaY-starY);
                GameObject.Find("Background").transform.Find("Spamer").GetComponent<Spammer>().SetStartY();
                GameObject.Find("Background").transform.Find("Spamer").GetComponent<Spammer>().SetSpam(areaY - starY);
                transform.parent.Find("Main Camera").GetComponent<follower>().SetStartY();
//                transform.parent.Find("Main Camera").localPosition -= new Vector3(0, areaY, 0);
                
            }
            lifeTime_now -= delta;
            if (lifeTime_now > 0)
            {
                GameObject go = GameObject.Find("Canvas UI");
                go = go.transform.Find("Oxygen").gameObject;
                go.GetComponent<Image>().fillAmount = startFill + (1 - startFill) * lifeTime_now / lifeTime;
                //go.transform.localScale = new Vector3(lifeTime_now / lifeTime, 1, 1);
            }
            else
            {
                GameObject.Find("Canvas UI").transform.Find("Oxygen").gameObject.GetComponent<Image>().fillAmount = 0;
                _state = State.Diying;
                gameObject.GetComponent<BoxCollider2D>().enabled = false;
                _dieTime = dieTime;
                foreach (Transform VARIABLE in GameObject.Find("Background").transform)
                {
                    if (VARIABLE.gameObject.name.StartsWith("Spamer"))
                    {
                        //VARIABLE.gameObject.GetComponent<Spammer>().enabled = false;
                        VARIABLE.gameObject.SetActive(false);
                    }
                }
                
                foreach (Transform VARIABLE in gameObject.transform.parent)
                {
                    if(VARIABLE.gameObject.name.StartsWith("Circle"))
                    {Destroy(VARIABLE.gameObject);}
                }
            }

            frameSpeed = transform.up * Speed * delta;
            transform.localPosition += new Vector3(frameSpeed.x, frameSpeed.y, 0);
            transform.parent.Find("Main Camera").GetComponent<follower>().SetY();
            
        }
        else if (_state == State.Diying)
        {
            if(_dieTime > 0)
            {
                _dieTime -= delta;
                var c = GetComponent<Image>().color;
                GetComponent<Image>().color = new Color(c.r, c.b, c.g,_dieTime / dieTime);
                foreach (Transform VARIABLE in GameObject.Find("Canvas UI").transform)
                {
                    if (VARIABLE.gameObject.name.StartsWith("Button") || VARIABLE.gameObject.name.StartsWith("Wheel"))
                    {
                        var _c = VARIABLE.gameObject.GetComponent<Image>().color;
                        VARIABLE.gameObject.GetComponent<Image>().color = new Color(_c.r, _c.b, _c.g,_dieTime / dieTime);
                        
                    }
                    
                }
            }
            else
            {
                var c = GetComponent<Image>().color;
                GetComponent<Image>().color = new Color(c.r, c.b, c.g,0);
                foreach (Transform VARIABLE in GameObject.Find("Canvas UI").transform)
                {
                    if (VARIABLE.gameObject.name.StartsWith("Button") || VARIABLE.gameObject.name.StartsWith("Wheel"))
                    {
                        var _c = VARIABLE.gameObject.GetComponent<Image>().color;
                        VARIABLE.gameObject.GetComponent<Image>().color = new Color(_c.r, _c.b, _c.g,0);
                        
                    }
                    
                }
                _state = State.Respauning;
                _respawnTime = respawnTime;

            }
            
        }
        else if (_state == State.Respauning)
        {
            if (_respawnTime > 0)
            {
                _respawnTime -= delta;
            }
            else
            {
                _state = State.Alive;
                lifeTime_now = lifeTime;
                GameObject.Find("Canvas UI").transform.Find("Oxygen").gameObject.GetComponent<Image>().fillAmount = 1;

                transform.localPosition = new Vector3(0, starY, transform.localPosition.z);
                transform.parent.Find("Main Camera").localPosition = new Vector3(0,0, transform.parent.Find("Main Camera").localPosition.z);;
                transform.parent.Find("Main Camera").GetComponent<follower>().SetStartY();// = new Vector3(0,0, transform.parent.Find("Main Camera").localPosition.z);;
                GameObject.Find("Canvas UI").transform.Find("Wheel").gameObject.GetComponent<Rotator>().SetAngle(0);
                GameObject.Find("Canvas UI").transform.Find("Wheel").rotation = Quaternion.Euler(Vector3.zero);
                transform.rotation = quaternion.Euler(Vector3.zero);
                foreach (Transform VARIABLE in GameObject.Find("Background").transform)
                {
                    if (VARIABLE.gameObject.name.StartsWith("Spamer"))
                    {
                        //VARIABLE.gameObject.GetComponent<Spammer>().enabled = true;
                        VARIABLE.gameObject.SetActive(true);
                    }
                }
                
                //GameObject.Find("Background").transform.Find("Spamer").GetComponent<Spammer>().enabled = true;
                gameObject.GetComponent<BoxCollider2D>().enabled = true;

                var c = GetComponent<Image>().color;
                GetComponent<Image>().color = new Color(c.r, c.b, c.g,1);
                foreach (Transform VARIABLE in GameObject.Find("Canvas UI").transform)
                {
                    if (VARIABLE.gameObject.name.StartsWith("Button") || VARIABLE.gameObject.name.StartsWith("Wheel"))
                    {
                        var _c = VARIABLE.gameObject.GetComponent<Image>().color;
                        VARIABLE.gameObject.GetComponent<Image>().color = new Color(_c.r, _c.b, _c.g,1);
                        
                    }
                    
                }

            }
        }

    }
}